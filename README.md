# Warhammer Fantasy Battle 6th edition BattleScribe Data

Warhammer Fantasy Battle 6th edition Battlescribe data for importing into the mobile app.

Using Ergofarg's data from <https://github.com/Ergofarg/Warhammer-Fantasy-6th-edition>

## Steps to import

1.  Open Manage Data.
2.  Click the three dots.
3.  Select "Add data index url".
4.  Enter the following url and click ok: <https://gitlab.com/philstephenson/whfb6thed/-/raw/main/index.bsi>
5.  Click the refresh Data icon.
6.  If you haven't already add the Warhammer Fantasy game repository in the usual way.
7.  Done. Warhammer Fantasy Battle 6th edition should now be an option when you create a new Warhammer Fantasy roster.
